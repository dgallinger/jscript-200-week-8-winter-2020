
let clr = 0;

function step() {
    if (clr < 255) {
        document.body.style.backgroundColor = `rgb(${clr},${clr},${clr})`;
        clr++;
        window.requestAnimationFrame(step);
    }
};

window.requestAnimationFrame(step);