const mathPromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    const num = Math.random();
    if (num > .5) {
      resolve(num);
    } else {
      reject();
    };
  }, 1000);
});

mathPromise.then(number => {
  console.log(`Success, ${number.toFixed(3)}`);
})
.catch(err => {
  console.log('Fail');
});

