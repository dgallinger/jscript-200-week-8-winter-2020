const formEl = document.getElementById('best-books-form');
const yearEl = document.getElementById('year');
const monthEl = document.getElementById('month');
const dateEl = document.getElementById('date');

formEl.addEventListener('submit', function(e) {
  e.preventDefault();

  const year = yearEl.value;
  const month = monthEl.value;
  const day = dateEl.value;

  // Fetch bestselling books for date and add top 5 to page
const BASE_URL = 'https://api.nytimes.com/svc/books/v3/lists';
const date = `${year}-${month}-${day}`;
const url = `${BASE_URL}/${date}/hardcover-fiction.json?api-key=${API_KEY_BESTSELLERS}`;

fetch(url)
  .then(function(data) {
    return data.json();
  })
  .then(function(responseJson) {
    console.log(responseJson);

    const topFive = responseJson.results.books.slice(0, 5);

    const divEl = document.getElementById('books-container');

    topFive.forEach((book) => {

      const lblTitle = document.createElement('p');
      const lblAuthor = document.createElement('ul');
      const lblDesc = document.createElement('ul');

      lblTitle.innerText = (`Title: ${book.title}`);
      lblAuthor.innerText = (`Author: ${book.author}`);
      lblDesc.innerText = (`Description: ${book.description}`);

      divEl.appendChild(lblTitle);
      divEl.appendChild(lblAuthor);
      divEl.appendChild(lblDesc);
            

    });

  });

});
