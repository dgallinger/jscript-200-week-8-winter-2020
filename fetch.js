// create api-key.js file with const API_KEY="your_api_key" in this same directory to use
const BASE_URL = 'https://api.nytimes.com/svc/search/v2/articlesearch.json';
//API_KEY = 'ijGPvTu1EyCMnULBLyFAKNlEF655wjUR';

const url = `${BASE_URL}?q=cars&api-key=${API_KEY}`;

fetch(url)
  .then(function(data) {
    return data.json();
  })
  .then(function(responseJson) {
    console.log(responseJson);

    let article = responseJson.response.docs[1];
    console.log(article);

    const mainHeadline = article.headline.main;
    document.getElementById('article-title').innerText = mainHeadline;

    if (article.multimedia.length > 0) {
      const imgUrl = `https://www.nytimes.com/${article.multimedia[0].url}`;
      document.getElementById('article-img').src = imgUrl;
    }

    if (article.abstract.length > 0) {
      const absUrl = `${article.abstract}`;
      document.getElementById('article-snippet').innerText = absUrl;
    }

    if (article.web_url.length > 0) {
      const webUrl = `${article.web_url}`;
      document.getElementById('article-link').href = webUrl;
    }

    if (article.byline.original.length >0) {
      const author = `Author: ${article.byline.original}`;
      document.getElementById('article-author').innerText = author;
    }
  });
