let clr = 255;
let darkenInterval = setInterval( () => {
    if (clr > 0) {
        document.querySelector('body').style.backgroundColor = `rgb(${clr},${clr},${clr})`;
        clr--;
    } else {
        clearInterval(darkenInterval);
    }
}, 500
);