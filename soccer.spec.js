describe('soccer functions', () => {
    describe('getTotalPoints', () => {
        it('Should equal 7 points for "wwdl"', () => {
            const points = getTotalPoints('wwdl');

            expect(points).toBe(7);
        });

        it('Should equal 5 points for "ddddd"', () => {
            const points = getTotalPoints('ddddd');

            expect(points).toBe(5);
        });
    });

    describe('orderTeams', () => {
        it('Should output correct team "name: points"', () => {
            const team = {
                name: 'Cougars',
                results: 'wdlw'
            };

            const output = orderTeams(team);
            expect(output).toBe('Cougars: 7');
        });
    });
});