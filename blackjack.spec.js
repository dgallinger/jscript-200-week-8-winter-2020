describe('Blackjack', () => {
    const ten = {
        val: 10,
        displayVal: '10'
    };
    const six = {
        val: 6,
        displayVal: '6'
    };
    const ace = {
        val: 11,
        displayVal: 'Ace'
    };
    const nine = {
        val: 9,
        displayVal: '9'
    };
    const two = {
        val: 2,
        displayVal: '2'
    };
    const four = {
        val: 4,
        displayVal: '4'
    };
    const five = {
        val: 5,
        displayVal: '5'
    };

    describe('calcPoints', () => {
     
        it('Handles standard numeric cards correctly', () => {
            const cardCount = calcPoints([ten, six]);
            const expected = {
                total: 16,
                isSoft: false
            };

            expect(cardCount).toEqual(expected);
        });

        it('Handles soft ace correctly', () => {
            const cardCount = calcPoints([ace, four, ten]);

            const expected = {
                total: 15,
                isSoft: true
            };

            expect(cardCount).toEqual(expected);
        });

        it('Handles hard ace correctly', () => {
            const cardCount = calcPoints([ace, six]);

            const expected = {
                total: 17,
                isSoft: true
            };

            expect(cardCount).toEqual(expected);
        });

        it('Handles multiple aces(soft) correctly', () => {
            const cardCount = calcPoints([ace, ace, six]);

            const expected = {
                total: 18,
                isSoft: true
            };

            expect(cardCount).toEqual(expected);
        });

        it('Handles multiple aces(hard) correctly', () => {
            const cardCount = calcPoints([ace, ace, six]);

            const expected = {
                total: 18,
                isSoft: false
            };

            expect(cardCount).toEqual(expected);
        });
    });
    
    describe('dealerShouldDraw', () => {
        it('Hand with 10, 9 should return false', () => {
            const toDraw = dealerShouldDraw([ten, nine]);

            expect(toDraw).toBe(false);
        });

        it('Hand with Ace, 6 should return false', () => {
            const toDraw = dealerShouldDraw([ace, six]);

            expect(toDraw).toBe(true);
        });

        it('Hand with 10, 6, Ace should return false', () => {
            const toDraw = dealerShouldDraw([ten, six, ace]);

            expect(toDraw).toBe(false);
        });

        it('Hand with 2, 4, 2, 5 should return false', () => {
            const toDraw = dealerShouldDraw([two, four, two, five]);

            expect(toDraw).toBe(true);
        });
    });
});